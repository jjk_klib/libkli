kli [![pipeline status](https://gitlab.com/jkushmaul/klibkli/badges/master/pipeline.svg)](https://gitlab.com/jkushmaul/klibkli/commits/master) [![coverage report](https://gitlab.com/jkushmaul/klibkli/badges/master/coverage.svg)](https://gitlab.com/jkushmaul/klibkli/commits/master)

The intent of this library is to provide a very simple command line interface, command line argument parsing in C.

karg
getopt seems difficult to get setup, or once you have the hang of it, so much repeated work.  
It's hard to keep help in sync with setup. This lets you build a tree of options, commands, and options that belong to only those commands.
There are many other fine solutions to this problem too.

Options: `./EXECUTABLE -v, --verbose | -h, --help, etc.`
Commands: `./EXECUTABLE init`
Commands w/ specific options: `./EXECUTABLE encrypt --keyid=0x01234567890`  (keyid might not have relevance in other commands)

It will return -1 on unkown option, missing required option, (both for global opts and command specific opts)
It will return 0 to indicate exit(0);
Use of commands is limited to only the first argument.
It will return optind to indicate the first non option after permuting the argv to move them to the end.

Use karg_option_register/karg_command_register funcs to build a tree
Use karg_parse_as_getopts

kli
I didn't find anything useful to add a shell like CLI to my projects.  This is very similar to the tree structure of karg.  
Eventually I'd say one should use the other but that is not the case today.

Currently has BSD2 licensed linenoise.c file with original notice.  Will resolve this later.

* Source: https://gitlab.com/jkushmaul/klibkli
* Docs: https://jkushmaul.gitlab.io/klibkli/index.html
* Coverage: https://jkushmaul.gitlab.io/klibkli/coverage/index.html
* Releases: https://gitlab.com/api/v4/projects/3618434/releases
** Ask gitlab to implement a UI for it.

TODO:
    * Autocomplete
    * Subcommands `git remote add http://repo.localhost origin`
WONT
    * non standard options such as -long
    * guessing of partial options
