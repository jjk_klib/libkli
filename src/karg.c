#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <karg.h>

void karg_init(karg_t *args)
{
	memset(args, 0, sizeof(karg_t));
}

void karg_release_frees(karg_t * args)
{
	klist_release_frees(&args->options);
	KLIST_FOR_EACH_NODE(node, (&args->commands)) {
		karg_command_t *cmd = node->value;

		klist_release_frees(&cmd->options);
	}
	klist_release_frees(&args->commands);
}

void karg_option_init(klist_t *options, karg_option_t *option, char short_opt, char *long_opt, char *help, int required, karg_option_callback_f callback, int takes_value)
{
	memset(option, 0, sizeof(karg_option_t));
	option->short_option = short_opt;
	option->long_option_len = strlen(long_opt);
	strncpy(option->long_option, long_opt, sizeof(option->long_option));
	strncpy(option->help, help, sizeof(option->help));
	option->required = required;
	option->callback = callback;
	option->takes_value = takes_value;
	klist_insert_value_allocs(options, NULL, option);
}

void karg_command_init(klist_t *commands, karg_command_t *cmd, char *name, char *help, karg_command_callback_f callback)
{
	memset(cmd, 0, sizeof(karg_command_t));
	strncpy(cmd->command, name, sizeof(cmd->command));
	strncpy(cmd->help, help, sizeof(cmd->help));
	cmd->callback = callback;
	klist_insert_value_allocs(commands, NULL, cmd);
}

karg_getopt_option_def *karg_build_getopts_allocs(karg_t *args)
{
	int optlen = args->options.count;
	int memsize = sizeof(karg_getopt_option_def) + sizeof(struct option) * (optlen + 1);
	karg_getopt_option_def *def = calloc(memsize, 1);

	def->optlen = optlen;

	size_t option_index = 0;
	size_t short_index = 0;
	//def->short_options[short_index++] = '-';        //maintain order
	def->short_options[short_index++] = ':';        //when an option is missing : will be returned.
	for (klist_node_t *node = args->options.head; node != NULL; node = node->next) {
		karg_option_t *kopt = node->value;
		def->short_options[short_index++] = kopt->short_option;
		struct option *getopt_opt = &def->long_options[option_index++];

		if (kopt->takes_value) {
			def->short_options[short_index++] = ':';
			getopt_opt->has_arg = 1;
		}

		getopt_opt->flag = 0;
		getopt_opt->name = kopt->long_option;
		getopt_opt->val = kopt->short_option;
	}
	//last option is all zeros

	return def;
}


int karg_add_error(karg_error_t *errors, char *fmt, ...)
{
	char *ptr = errors->error_msg;
	size_t plen = sizeof(errors->error_msg);

	if (errors->has_error++) {
		ptr = errors->error_msg;
		size_t elen = strlen(ptr);
		plen -= elen;
		snprintf(ptr++, plen--, "\n");
	}
	va_list args;
	va_start(args, fmt);
	vsnprintf(ptr, plen, fmt, args);
	va_end(args);
	return 0;
}

karg_command_t *karg_find_command(klist_t *commands, char *arg)
{
	for (klist_node_t *node = commands->head; node != NULL; node = node->next) {
		karg_command_t *kmd = node->value;
		if (!strcmp(kmd->command, arg)) {
			return kmd;
		}
	}
	return NULL;
}

karg_option_t *karg_find_option(klist_t *options, char short_option)
{
	for (klist_node_t *node = options->head; node != NULL; node = node->next) {
		karg_option_t *kopt = node->value;
		if (short_option == kopt->short_option) {
			return kopt;
		}
	}
	return NULL;
}

//build it first.
//0 means exit(0)
//<0 means error
//>0 returns next index of argv to process
int karg_parse_args(karg_t *args, karg_getopt_option_def *def, int argc, char **argv)
{
	int option_index = 0;


	//reset getopt if needed:
	opterr = 0;
	optind = 1;

	int c = 0;
	karg_command_t *current_cmd = NULL;
	if (args->commands.count > 0) {
		if (argc > 1 && argv[1][0] != '-') {
			//should be a command?  unsure
			current_cmd = karg_find_command(&args->commands, argv[1]);
			if (current_cmd == NULL) {
				karg_add_error(&args->errors, "Invalid command %s", argv[optind - 1]);
				return -1;
			} else {
				optind++;
			}
		}
	}
	c = 0;
	while ((c = getopt_long((int)argc, (char**)argv, def->short_options,
				def->long_options, &option_index)) != -1) {

		switch (c) {
		case '?':   //an unknown option was provided
			karg_add_error(&args->errors, "Invalid option: %s", argv[optind - 1]);
			return -1;
		case ':':   //an option that takes a value is missing the value.
			karg_add_error(&args->errors, "option %s requires a value", argv[optind - 1]);
			return -1;
		case -1: // -1 means all command-line options have been parsed..
			break;
		case 'h':
			karg_print_help(args);
			//indicate to exit - 0 is special in that we don't want error code but to exit
			return 0;
		default:
		{
			karg_option_t *kopt = NULL;
			if (current_cmd != NULL) {
				kopt = karg_find_option(&current_cmd->options, c);
			}
			if (kopt == NULL) {
				kopt = karg_find_option(&args->options, c);
			}

			if (kopt != NULL) {
				kopt->argv_index = optind - 1;
				if (kopt->takes_value && !optarg) {
					karg_add_error(&args->errors, "Option %s requires a value", argv[optind - 1]);
					return -1;
				} else if (!kopt->takes_value && optarg) {
					karg_add_error(&args->errors, "Option %s does not take a value", argv[optind - 1]);
					return -1;
				} else if (kopt->takes_value) {
					int result = karg_option_handle_value(args, kopt, optarg);
					if (result < 0) {
						karg_add_error(&args->errors, "Invalid value for option: %s", argv[optind - 1]);
						return -1;
					}
				}
			}

			if (kopt == NULL) {
				karg_add_error(&args->errors, "Unexpected option: %s", argv[optind - 1]);
				return -1;
			}
			break;
		}
		}
	}
	return optind;
}

int karg_option_handle_value(karg_t *args, karg_option_t *opt, char *argv)
{
	int result = 0;

	if (opt->callback) {
		result = opt->callback(args, opt, argv);
	} else {
		opt->value = argv;
	}
	return result;
}

int karg_validate_opts(karg_t *args, klist_t *options)
{
	KLIST_FOR_EACH_NODE(node, options){
		karg_option_t *opt = node->value;

		if (opt->argv_index == 0 && opt->required) {
			if (args->errors.has_error) {
				snprintf(args->errors.error_msg, sizeof(args->errors.error_msg), "\noption -%c, --%s is required", opt->short_option, opt->long_option);
			} else {
				args->errors.has_error = 1;
				snprintf(args->errors.error_msg, sizeof(args->errors.error_msg), "option -%c, --%s is required", opt->short_option, opt->long_option);
			}

			return -1;
		}
	}
	return 0;
}

int karg_validate_args(karg_t *args)
{
	int test = karg_validate_opts(args, &args->options);

	if (!test) {
		KLIST_FOR_EACH_NODE(node, (&args->commands)) {
			karg_command_t *cmd = node->value;

			if (cmd->argv_index > 0) {
				test = karg_validate_opts(args, &cmd->options);
				if (test) {
					break;
				}
			}
		}
	}
	return test;
}

void karg_print_options_help(int padding, klist_t *options)
{
	if (options->head != NULL) {
		printf("%.*s", padding, "                ");
		printf("Options:\n");
		padding += 3;
		KLIST_FOR_EACH_NODE(opt_node, options){
			karg_option_t *opt = opt_node->value;

			char *format_takes_value = "=VALUE";
			char *format_no_value = "";

			char *value;

			if (opt->takes_value) {
				value = format_takes_value;
			} else {
				value = format_no_value;
			}

			char argline[256];
			memset(argline, 0, sizeof(argline));
			if (opt->required) {
				snprintf(argline, sizeof(argline), " -%c, --%s%s ", opt->short_option, opt->long_option, value);
			} else {
				snprintf(argline, sizeof(argline), "[-%c, --%s%s]", opt->short_option, opt->long_option, value);
			}

			char *format = "%.*s%-35s   %-60s\n";
			printf(format, padding, "                ", argline, opt->help);
		}
	}
}

void karg_print_commands_help(int padding, klist_t *commands)
{
	if (commands->head != NULL) {

		printf("%.*s", padding, "                ");
		printf("Commands:\n");
		padding += 3;
		KLIST_FOR_EACH_NODE(com_node, commands) {
			karg_command_t *com = com_node->value;

			printf("%.*s", padding, "                ");
			printf("%s: %s\n", com->command, com->help);
			karg_print_options_help(padding, &com->options);
		}
	}
}

void karg_print_help(karg_t *arg)
{
	printf("Usage: %s [OPTIONS] [COMMANDS]\n", arg->program_name);
	if (arg->errors.has_error) {
		printf("Errors: %s\n", arg->errors.error_msg);
	}
	karg_print_commands_help(0, &arg->commands);
	karg_print_options_help(0, &arg->options);
	printf("Report bugs to: %s\n", arg->author);
}
