#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include <kstring.h>
#include "konfig2.h"



void konfig2_init_allocs(konfig2_t *konfig, int table_size)
{
	bzero(konfig, sizeof(konfig2_t));
	konfig->config_map = khashmap_new_allocs(table_size);

}
void konfig2_release_frees(konfig2_t *konfig)
{
	khashmap_entry_t *entry = NULL;

	if (konfig->config_map != NULL) {
		KHASHMAP_FOR_EACH_VALUE(konfig->config_map, entry) {
			//nothing to free in var.
			free(entry->value);
		}
		khashmap_release_frees(konfig->config_map);

		konfig->config_map = NULL;
	}
}

konfig2_var_t *konfig2_get_var(konfig2_t *konfig, char *key)
{
	khashmap_key_t keystruct = { 0 };

	keystruct.data = (uint8_t*)key;
	keystruct.len = strlen(key);
	return khashmap_get(konfig->config_map, &keystruct);
}

konfig2_var_t *konfig2_var_new_allocs(konfig2_t *konfig, char *key, uint8_t *data, size_t data_len)
{
	konfig2_var_t *var = calloc(1, sizeof(konfig2_var_t));

	snprintf(var->key, sizeof(var->key), "%s", key);
	var->data = (uint8_t*)data;
	var->data_len = data_len;

	khashmap_key_t keystruct = { 0 };
	keystruct.data = (uint8_t*)var->key;
	keystruct.len = strlen(var->key);
	khashmap_put_allocs(konfig->config_map, &keystruct, var);

	return var;
}
konfig2_var_t *konfig2_var_new_string_allocs(konfig2_t *konfig, char *key, char *data, size_t data_len)
{
	konfig2_var_t *var = konfig2_var_new_allocs(konfig, key, (uint8_t*)data, data_len);

	var->parse = konfig2_parse_string;
	var->tostring = konfig2_tostring_string_allocs;
	return var;
}
konfig2_var_t *konfig2_var_new_double_allocs(konfig2_t *konfig, char *key, double *data)
{
	konfig2_var_t *var = konfig2_var_new_allocs(konfig, key, (uint8_t*)data, sizeof(double));

	var->parse = konfig2_parse_double;
	var->tostring = konfig2_tostring_double_allocs;
	return var;
}
konfig2_var_t *konfig2_var_new_long_allocs(konfig2_t *konfig, char *key, long *data)
{
	konfig2_var_t *var = konfig2_var_new_allocs(konfig, key, (uint8_t*)data, sizeof(long));

	var->parse = konfig2_parse_long;
	var->tostring = konfig2_tostring_long_allocs;
	return var;
}

int konfig2_parse_string(konfig2_var_t *var, char *value)
{
	int len = strlen(value);

	if (len > var->data_len - 1) {
		return -1;
	}
	char *data = (char*)var->data;
	data[0] = '\0';
	snprintf(data, var->data_len, "%s", value);
	return 0;
}
int konfig2_parse_double(konfig2_var_t *var, char *value)
{
	errno = 0;

	double d = strtod(value, NULL);

	if (errno != 0) {
		return -1;
	} else {
		*((double*)var->data) = d;
		return 0;
	}
}
int konfig2_parse_long(konfig2_var_t *var, char *value)
{
	errno = 0;

	long d = strtol(value, NULL, 10);

	if (errno != 0) {
		return -1;
	} else {
		*((long*)var->data) = d;
		return 0;
	}
}

char *konfig2_tostring_string_allocs(konfig2_var_t *var)
{
	char *result = NULL;

	char *data = (char*)var->data;

	if (kstring_sprintf_allocs(&result, "%s", data) == (size_t)-1) {
		return NULL;
	}

	return result;
}
char *konfig2_tostring_double_allocs(konfig2_var_t *var)
{
	char *result = NULL;
	double data = *((double*)var->data);

	if (kstring_sprintf_allocs(&result, "%lf",  data) == (size_t)-1) {
		return NULL;
	}

	return result;
}
char *konfig2_tostring_long_allocs(konfig2_var_t *var)
{
	char *result = NULL;
	long data = *((long*)var->data);

	if (kstring_sprintf_allocs(&result, "%ld", data) == (size_t)-1) {
		return NULL;
	}

	return result;
}

int konfig2_parse_document_allocs(konfig2_t *konfig, char *buffer, const size_t len)
{
	int result = 0;
	int count = 0;
	char **lines = kstring_split_allocs(buffer, "\n", &count);

	if (lines) {
		for (int i = 0; i < count; i++) {
			char *line = lines[i];
			if (konfig2_parse_line_allocs(konfig, line, strlen(line))) {
				result = -1;
			}
			free(line);
		}
		free(lines);
	} else {
		result = -1;
	}

	return result;
}

int konfig2_parse_line_allocs(konfig2_t *konfig, char *line, size_t line_len)
{
	int count = 0;
	char **result = kstring_split_allocs(line, "=", &count);
	int retval = -1;

	if (count == 2) {
		konfig2_var_t *var = konfig2_get_var(konfig, result[0]);
		if (var != NULL) {
			retval = var->parse(var, result[1]);
		}
	}

	if (result) {
		for (int i = 0; i < count; i++) {
			free(result[i]);
		}
		free(result);
	}
	return retval;
}
char *konfig2_dump(konfig2_t *konfig)
{
	char *buffer = (char*)calloc(1, 1);
	khashmap_entry_t *entry = NULL;

	KHASHMAP_FOR_EACH_VALUE(konfig->config_map, entry) {
		konfig2_var_t *var = (konfig2_var_t*)entry->value;
		char *val = var->tostring(var);
		char *tmp = NULL;

		kstring_sprintf_allocs(&tmp, "%s%s=%s\n", buffer, var->key, val);
		free(val);
		if (buffer) {
			free(buffer);
		}
		buffer = tmp;
	}
	return buffer;
}
