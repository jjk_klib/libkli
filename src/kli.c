
#define _GNU_SOURCE 1

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>

#include <linenoise/linenoise.h>
#include <kstring.h>
#include "kli.h"

/**
 * @brief iterates orig_argv freeing, and frees itself
 *
 * @param exec p_exec:...
 */
void kli_release_exec_frees(kli_exec_t *exec)
{
	if (exec->orig_argv) {
		for (int i = 0; i < exec->orig_argc; i++) {
			free(exec->orig_argv[i]);
		}
		free(exec->orig_argv);
	}
}


void kli_release_command_frees(kli_t *kli, kli_com_t *com)
{
	if (com != NULL) {
		KLIST_FOR_EACH_NODE(node, (&com->child_list)) {
			kli_com_t *value = (kli_com_t*)node->value;

			kli_release_command_frees(kli, value);
			free(value);
		}
		klist_release_frees(&com->child_list);
	}
}


kli_com_t *kli_register_command_allocs(kli_t *cli, kli_com_t *parent, const char *command, const char *help, kli_action_f action, void *context)
{
	kli_com_t *cmd = calloc(1, sizeof(kli_com_t));

	cmd->action.func = action;
	cmd->action.context = context;
	strncpy(cmd->command, command, sizeof(cmd->command));
	strncpy(cmd->help, help, sizeof(cmd->help));

	if (!parent) {
		parent = &cli->root_command;
	}
	klist_insert_value_allocs(&parent->child_list, NULL, cmd);
	return cmd;
}

static void SignalHandler(int signum)
{
	//only used to interrupt the read in linenoise.
}


void kli_init(kli_t *kli)
{
	memset(kli, 0, sizeof(kli_t));
	kli->read_line_allocs = kli_prompt_allocs;

	kli->destroy_mutexes = -1;
	pthread_mutex_init(&kli->input_thread_mutex, NULL);
	pthread_mutex_init(&kli->stdin_mutex, NULL);
	pthread_cond_init(&kli->line_input_cond, NULL);

	struct sigaction action;
	memset(&action, 0, sizeof(action)); // SA_RESTART bit not set
	action.sa_handler = SignalHandler;
	sigaction(SIGUSR1, &action, NULL);
}

int kli_start_input_thread(kli_t *kli)
{
	kli->kill_thread = 1;
	return pthread_create(&kli->input_thread, NULL, kli_thread, (void*)kli);

}

void kli_shutdown(kli_t *kli)
{
	kli->shutdown = 1;
	if (kli->destroy_mutexes || kli->kill_thread) {
		if (kli->kill_thread) {
			pthread_kill(kli->input_thread, SIGUSR1);       //bust loose the read operation
			pthread_mutex_lock(&kli->input_thread_mutex);   //wait until it exits.

			struct timespec ts;
			if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
				/* Handle error */
				//printf("Could not clock_getttime: %s\n", strerror(errno));
				pthread_kill(kli->input_thread, SIGKILL);
			} else {
				ts.tv_sec += 5;
				//printf("Performing 5 second timed join on mykli\n");
				if (pthread_timedjoin_np(kli->input_thread, NULL, &ts) != 0) {
					//printf("Could not timedjoin thread, cancelling\n");
					pthread_kill(kli->input_thread, SIGKILL);
					//printf("joining\n");
					pthread_join(kli->input_thread, NULL);//read is interrupted allow it to finish to free.
				}
			}
			kli->kill_thread = 0;
		}

		pthread_mutex_destroy(&kli->input_thread_mutex);
		pthread_mutex_destroy(&kli->stdin_mutex);
		pthread_cond_destroy(&kli->line_input_cond);
		kli->destroy_mutexes = 0;
	}

	kli_release_command_frees(kli, &kli->root_command);
	if (kli->line_input != NULL) {
		free(kli->line_input);
		kli->line_input = NULL;
	}
}


void *kli_thread(void * args)
{
	kli_t *kli = (kli_t*)args;

	pthread_mutex_lock(&kli->input_thread_mutex);

	while (!kli->shutdown) {
		pthread_mutex_lock(&kli->stdin_mutex);
		kli->line_input = kli_prompt_allocs(kli);
		if (kli->line_input == NULL) {
			//avoid a deadlock - this is due to a CTL-C
            //this is better to me than using a global.
			kli->line_input = strdup("");
		}
		//want to wait on condition set by calling thread that it has finished.
		pthread_cond_wait(&kli->line_input_cond, &kli->stdin_mutex);
		pthread_mutex_unlock(&kli->stdin_mutex);
	}

	pthread_mutex_unlock(&kli->input_thread_mutex);
	return NULL;
}

void kli_process_thread_input(kli_t *kli)
{
	if (kli->line_input != NULL) {
		pthread_mutex_lock(&kli->stdin_mutex);
        //ensure this wasn't due to a SIGINT
		if (kli->line_input[0] != '\0') {
			//don't process empty lines.
			kli_error_t error = { 0 };
			kli_process_line(kli, kli->line_input, &error);
			if (error.has_error) {
				printf("%s\n", error.error_msg);
			}
		}
		free(kli->line_input);
		kli->line_input = NULL;
		pthread_mutex_unlock(&kli->stdin_mutex);
		pthread_cond_signal(&kli->line_input_cond);
	}
}


char *kli_prompt_allocs(kli_t *cli)
{
	const char *prompt;

	if (cli->prompt == NULL) {
		prompt = ">";
	} else {
		prompt = cli->prompt;
	}

	char *line = linenoise(prompt);

	if (line) {
		linenoiseHistoryAdd(line);
	}
	return line;
}

int kli_process_line(kli_t *cli, char *line, kli_error_t *error)
{
	kli_exec_t exec = { 0 };
	kli_com_t *com = kli_parse_line_allocs(cli, &exec, line, error);
	int result = -1;

	if (com != NULL && com != &cli->root_command && exec.command && exec.command->action.func) {
		result = exec.command->action.func(cli, exec.command->action.context, &exec);
	} else {
		sprintf(error->error_msg, "Unknown command");
		error->has_error = 1;
	}

	kli_release_exec_frees(&exec);

	return result;
}

int kli_compare_command(void *a, void *b)
{
	kli_com_t *ac = a;
	kli_com_t *bc = b;

	return strcmp(ac->command, bc->command);
}

kli_com_t *kli_get_matching_child_command(char *arg, kli_com_t *parent)
{
	kli_com_t needle;

	strncpy(needle.command, arg, sizeof(needle.command));
	klist_node_t *node = klist_find_node_by_comparison(&parent->child_list, &needle, kli_compare_command);
	if (node) {
		return node->value;
	} else {
		return NULL;
	}
}

kli_com_t *kli_parse_line_allocs(kli_t *cli, kli_exec_t *exec, char *line, kli_error_t *error)
{
	int argc = 0;

	memset(exec, 0, sizeof(kli_exec_t));
	char **argv = kstring_split_allocs(line, " ", &argc);
	exec->orig_argc = argc;
	exec->orig_argv = argv;

	kli_com_t *last_cmd = &cli->root_command;
	for (int j = 0; j < argc; j++) {
		kli_com_t *c = kli_get_matching_child_command(argv[j], last_cmd);
		if (c != 0) {
			last_cmd = c;
			if (c->child_list.count == 0) {
				exec->argc = argc - 1 - j;
				if (j < argc - 1) {
					exec->argv = argv + j + 1;
				}
				exec->command = c;
				break;
			} else if (j == argc - 1) {
				//should be more commands
				if (error) {
					if (error->has_error) {
						size_t slen = strlen(error->error_msg);
						snprintf(error->error_msg + slen, sizeof(error->error_msg) - slen, "\nIncomplete command, %s", last_cmd->help);
					} else {
						snprintf(error->error_msg, sizeof(error->error_msg), "Incomplete command, %s", last_cmd->help);
						error->has_error = 1;
					}

				}
				break;
			}
		}
	}
	if (last_cmd == 0) {
		if (error) {
			if (error->has_error) {
				size_t slen = strlen(error->error_msg);
				snprintf(error->error_msg + slen, sizeof(error->error_msg) - slen, "\nUnknown command");
			} else {
				snprintf(error->error_msg, sizeof(error->error_msg), "Unknown command");
				error->has_error = 1;
			}

		}
	}
	return last_cmd;
}

void kli_recurse_print_commands(kli_t *cli, kli_com_t *com, int tabs)
{
	if (com != &cli->root_command) {
		printf("%.*s%-35s %-35s\n", tabs * 4, "                              ", com->command, com->help);
		tabs++;
	}
	KLIST_FOR_EACH_NODE(node, (&com->child_list)){
		kli_com_t *child = (kli_com_t*)node->value;

		kli_recurse_print_commands(cli, child, tabs + 1);
	}
}
