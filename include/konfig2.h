#ifndef KONFIG2_H
#define KONFIG2_H


/**
 * konfig2 is a hashmap of config options that can be read/stored in a text file.
 * Very simple:
 * key1=value\n
 * key2=2.0\n
 * key3=3\n
 *
 * The key of the hashmap is the property name.
 * The value of the hashmap is a konfig2_var_t
 *
 * konfig2_var_t contains
 * key
 * uint8_t *data
 * size_t len
 * parse function
 * tostring function
 *
 *
 *
 * To define a konfig2
 * struct myconfig_struct {
 *   char key1[1024];
 *   float key2;
 *   int key3;
 * }
 * struct myconfig_struct internal_config;
 * //any defaults set here, if they are not read will remain as defaults
 *
 * konfig2_t *konfig = konfig2_new_allocs(10);
 * konfig2_new_var_allocs(konfig, "key1", &internal_config.key1, sizeof(internal_config.key1), KONFIG_VAR_TYPE_STRING)
 * konfig2_new_var_allocs(konfig, "key2", &internal_config.key1, sizeof(internal_config.key1), KONFIG_VAR_TYPE_FLOAT)
 * konfig2_new_var_allocs(konfig, "key3", &internal_config.key1, sizeof(internal_config.key1), KONFIG_VAR_TYPE_INT)
 *
 * I'd like to try and make a #define to aid in this, but save that for later.
 * KONFIG_NEW_VAR(konfig, "key1", internal_config.key)
 *
 * custom parsing/tostring:
 * konfig2_var_t *var = konfig2_new_var_allocs(konfig, "key4", &internal_config.key1, sizeof(internal_config.key1), KONFIG_VAR_TYPE_CUSTOM)
 * var.parse = my_custom_parse
 * var.tostring = my_custom_tostring
 *
 *
 *
 *
 * In the above cases konfig2_new_var_allocs will look at type given, and use premade parse/tostring functions for the 3 types.
 * If custom it leaves them as null.
 *
 * When the var is parsed, the parse function is called and the ptr is set.
 * When the var is dumped, the tostring function is called and the ptr is encoded into a string.
 *
 * The internal_config is then used by application as normal.
 *
 **/
#include <stdint.h>
#include <stdlib.h>

#include <khashmap.h>

typedef struct konfig2_t {
	khashmap_t *config_map;
} konfig2_t;


typedef struct konfig2_var_t konfig2_var_t;
typedef int (*konfig2_parse_f)(konfig2_var_t *var, char *value);
typedef char *(*konfig2_tostring_f)(konfig2_var_t *var);

struct konfig2_var_t {
	char key[256];
	uint8_t *data;
	size_t data_len;
	konfig2_parse_f parse;
	konfig2_tostring_f tostring;
};



void konfig2_init_allocs(konfig2_t *konfig, int table_size);
void konfig2_release_frees(konfig2_t *konfig);
konfig2_var_t *konfig2_get_var(konfig2_t *konfig, char *key);

konfig2_var_t *konfig2_var_new_allocs(konfig2_t *konfig, char *key, uint8_t *data, size_t data_len);
konfig2_var_t*konfig2_var_new_string_allocs(konfig2_t *konfig, char *key, char *data, size_t data_len);
konfig2_var_t* konfig2_var_new_double_allocs(konfig2_t *konfig, char *key, double *data);
konfig2_var_t*konfig2_var_new_long_allocs(konfig2_t *konfig, char *key, long *data);

int konfig2_parse_string(konfig2_var_t *var, char *value);
int konfig2_parse_double(konfig2_var_t *var, char *value);
int konfig2_parse_long(konfig2_var_t *var, char *value);

char *konfig2_tostring_string_allocs(konfig2_var_t *var);
char *konfig2_tostring_double_allocs(konfig2_var_t *var);
char *konfig2_tostring_long_allocs(konfig2_var_t *var);


int konfig2_parse_document_allocs(konfig2_t *konfig, char *buffer, const size_t len);
int konfig2_parse_line_allocs(konfig2_t *konfig, char *line, size_t line_len);
char *konfig2_dump(konfig2_t *konfig);
#endif
