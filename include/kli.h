/**
 * @file kli.h
 * @author Jason Kushmaul
 * @date 9 Sep 2017
 * @brief Small quick embeddable CLI
 * @see https://klib.gitlab.io/libkli/
 */

#ifndef KLI_H
#define KLI_H
#include <stdlib.h>
#include <klist.h>
#include <pthread.h>

typedef struct kli_error_t {
	int has_error;
	char error_msg[1024];
} kli_error_t;

struct kli_t;
typedef struct kli_t kli_t;

struct kli_com_t;
typedef struct kli_com_t kli_com_t;

struct kli_exec_t;
typedef struct kli_exec_t kli_exec_t;


typedef int (*kli_action_f)(kli_t *cli, void *context, kli_exec_t *exec);

typedef struct {
	void *context;
	kli_action_f func;
} kli_callback_t;
/**
 * @brief A kli command definition
 *
 */
typedef struct kli_com_t {
	char command[32];
	char help[256];
	kli_callback_t action;
	size_t min_args;
	size_t max_args;
	klist_t child_list;
} kli_com_t;


typedef char* (*kli_readline_allocs_f)(kli_t* cli);

/**
 * @brief Kli container for definitions
 *
 */
typedef struct kli_t {
	int shutdown;
	kli_readline_allocs_f read_line_allocs;
	char *line_input;

	//threads can only be destroyed on shutdown if this is ==-1
	int destroy_mutexes;
	int kill_thread;
	pthread_t input_thread;
	pthread_cond_t line_input_cond;
	pthread_mutex_t stdin_mutex;
	pthread_mutex_t input_thread_mutex;
	kli_com_t root_command;
	char *prompt;
} kli_t;


/**
 * @brief Used to store an execution context from a parsed line
 *
 */
#define KLI_EXEC_T_SIZE sizeof(kli_exec_t)
typedef struct kli_exec_t {
	kli_com_t *command;
	int argc;
	char **argv;
	int orig_argc;
	char **orig_argv;
} kli_exec_t;

//kli_com_t  show_tables = {command: "tables", help: "show tables", children: {0}};
//kli_com_t show = {command: "show", help: "Show (tables|nodes|buckets)", children: {&show_tables}};

/**
 * @brief allocates and adds command to parent, if parent NULL sets root.
 *
 * @param cli p_cli:...
 * @param parent p_parent:...
 * @param command p_command:...
 * @param help p_help:...
 * @param action p_action:...
 * @return kli_com_t*
 */
kli_com_t *kli_register_command_allocs(kli_t *cli, kli_com_t *parent, const char *command, const char *help, kli_action_f action, void *context);

/**
 * @brief Event loop that can be used with pthread
 *
 * @param args p_args:...
 * @return void*
 */
void* kli_thread(void *args);

/**
 * @brief Single event
 *
 * @param cli p_cli:...
 */
void kli_loop(kli_t *cli, kli_error_t *error);

/**
 * @brief Given a line will allocate and populate command, populates exec
 *
 * @param cli p_cli:...
 * @param exec p_exec:An allocated exec_t, it should be released.
 * @param line p_line:...
 * @return kli_com_t*
 */
kli_com_t *kli_parse_line_allocs(kli_t *cli, kli_exec_t *exec, char *line, kli_error_t *error);

/**
 * @brief Finds the child that has matching command
 *
 * @param arg p_arg:...
 * @param parent p_parent:...
 * @return kli_com_t*
 */
kli_com_t *kli_get_matching_child_command(char *arg, kli_com_t *parent);

/**
 * @brief Just a standard way to allocate
 *
 * @return kli_t*
 */
void kli_init(kli_t *kli);


/**
 * @brief iterates children and frees unless it is the root
 * up to caller to free root com (or not to)
 *
 * @param kli p_kli:...
 * @param com p_com:...
 */
void kli_release_command_frees(kli_t *kli, kli_com_t *com);


/**
 * @brief klist comparator for command field
 *
 * @param a p_a:...
 * @param b p_b:...
 * @return int
 */
int kli_compare_command(void *a, void *b);

void kli_release_exec_frees(kli_exec_t *exec);


void kli_recurse_print_commands(kli_t *cli, kli_com_t *com, int tabs);


char *kli_prompt_allocs(kli_t* cli);

int kli_process_line(kli_t *cli, char *line, kli_error_t *error);
void kli_process_thread_input(kli_t *kli);
int kli_start_input_thread(kli_t *kli);
void kli_shutdown(kli_t *kli);
#endif
