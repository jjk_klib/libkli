#include <gtest/gtest.h>
#include <gmock/gmock.h>

using namespace testing;

#include <atomic>
using namespace std;

extern "C" {
    #include <konfig2.h>
}

TEST(konfig2, konfig2_init_allocs) {
	konfig2_t konfig = { 0 };

	konfig2_init_allocs(&konfig, 1000);
	ASSERT_NE((khashmap_t*)NULL, konfig.config_map);
	konfig2_release_frees(&konfig);
}
TEST(konfig2, konfig2_release_frees_initialized) {
	konfig2_t konfig = { 0 };

	konfig2_init_allocs(&konfig, 1000);
	konfig2_release_frees(&konfig);
	ASSERT_EQ((khashmap_t*)NULL, konfig.config_map);
}
TEST(konfig2, konfig2_release_frees_zeroed) {
	konfig2_t konfig;

	bzero(&konfig, sizeof(konfig2_t));
	konfig2_release_frees(&konfig);
	ASSERT_EQ((khashmap_t*)NULL, konfig.config_map);
}

TEST(konfig2, konfig2_var_new_allocs) {
	konfig2_t konfig = { 0 };

	konfig2_init_allocs(&konfig, 1000);

	const char *key = "mykey";
	const char *data = "mydata";
	size_t data_len = strlen(data);


	konfig2_var_t * var = konfig2_var_new_allocs(&konfig, (char*)key, (uint8_t*)data, data_len);

	ASSERT_NE((konfig2_var_t* )NULL, var);
	ASSERT_EQ(0, strcmp(var->key, key));
	ASSERT_EQ((uint8_t*)data, var->data);
	ASSERT_EQ(data_len, var->data_len);


	konfig2_release_frees(&konfig);
}
TEST(konfig2, konfig2_var_new_string_allocs) {
	konfig2_t konfig = { 0 };

	konfig2_init_allocs(&konfig, 1000);

	const char *key = "mykey";
	const char *data = "mydata";
	size_t data_len = strlen(data);


	konfig2_var_t * var = konfig2_var_new_string_allocs(&konfig, (char*)key, (char*)data, data_len);

	ASSERT_NE((konfig2_var_t* )NULL, var);
	ASSERT_EQ(0, strcmp(var->key, key));
	ASSERT_EQ((uint8_t*)data, var->data);
	ASSERT_EQ(data_len, var->data_len);
	ASSERT_EQ(konfig2_parse_string, var->parse);
	ASSERT_EQ(konfig2_tostring_string_allocs, var->tostring);


	konfig2_release_frees(&konfig);
}
TEST(konfig2, konfig2_var_new_double_allocs) {
	konfig2_t konfig = { 0 };

	konfig2_init_allocs(&konfig, 1000);

	const char *key = "mykey";
	double data = 42.1;
	size_t data_len = sizeof(data);


	konfig2_var_t * var = konfig2_var_new_double_allocs(&konfig, (char*)key, &data);

	ASSERT_NE((konfig2_var_t* )NULL, var);
	ASSERT_EQ(0, strcmp(var->key, key));
	ASSERT_EQ((uint8_t*)&data, var->data);
	ASSERT_EQ(data_len, var->data_len);
	ASSERT_EQ(konfig2_parse_double, var->parse);
	ASSERT_EQ(konfig2_tostring_double_allocs, var->tostring);


	konfig2_release_frees(&konfig);
}
TEST(konfig2, konfig2_var_new_long_allocs) {
	konfig2_t konfig = { 0 };

	konfig2_init_allocs(&konfig, 1000);

	const char *key = "mykey";
	long data = 42;
	size_t data_len = sizeof(data);


	konfig2_var_t * var = konfig2_var_new_long_allocs(&konfig, (char*)key, &data);

	ASSERT_NE((konfig2_var_t* )NULL, var);
	ASSERT_EQ(0, strcmp(var->key, key));
	ASSERT_EQ((uint8_t*)&data, var->data);
	ASSERT_EQ(data_len, var->data_len);
	ASSERT_EQ(konfig2_parse_long, var->parse);
	ASSERT_EQ(konfig2_tostring_long_allocs, var->tostring);


	konfig2_release_frees(&konfig);
}
TEST(konfig2, konfig2_tostring_long_allocs) {
	konfig2_var_t var = {};
	long i = 42;

	var.data = (uint8_t*)&i;
	var.data_len = sizeof(long);
	char *val = konfig2_tostring_long_allocs(&var);
	ASSERT_EQ(0, strcmp(val, "42"));
	free(val);
}
TEST(konfig2, konfig2_tostring_string_allocs) {
	konfig2_var_t var = { };
	const char *i = "abcde";

	var.data = (uint8_t*)i;
	var.data_len = 6;
	char *val = konfig2_tostring_string_allocs(&var);
	ASSERT_EQ(0, strcmp(val, "abcde"));
	free(val);
}
TEST(konfig2, konfig2_tostring_double_allocs) {
	konfig2_var_t var = { };
	double i = 42;

	var.data = (uint8_t*)&i;
	var.data_len = sizeof(double);
	char *val = konfig2_tostring_double_allocs(&var);
	ASSERT_EQ(0, strcmp(val, "42.000000"));
	free(val);
}
TEST(konfig2, konfig2_get_var) {
	konfig2_t konfig = { 0 };

	konfig2_init_allocs(&konfig, 1000);

	const char *key = "mykey";
	long data = 42;

	konfig2_var_t * var1 = konfig2_var_new_long_allocs(&konfig, (char*)key, &data);
	konfig2_var_t * var2 = konfig2_get_var(&konfig, (char*)key);
	ASSERT_EQ(var1, var2);

	konfig2_release_frees(&konfig);
}
TEST(konfig2, konfig2_dump) {
	konfig2_t konfig = { 0 };

	konfig2_init_allocs(&konfig, 1000);

	const char *key1 = "key1";
	long data1 = 42;
	konfig2_var_new_long_allocs(&konfig, (char*)key1, &data1);
	const char *key2 = "key2";
	double data2 = 42.3;
	konfig2_var_new_double_allocs(&konfig, (char*)key2, &data2);
	const char *key3 = "key3";
	const char *data3 = "fortytwo";
	konfig2_var_new_string_allocs(&konfig, (char*)key3, (char*)data3, strlen(data3));

	char *buffer = konfig2_dump(&konfig);
	ASSERT_NE((char*)NULL, buffer);
	const char *check = "key2=42.300000\nkey3=fortytwo\nkey1=42\n";
	ASSERT_EQ(0, strcmp(check, buffer));


	konfig2_release_frees(&konfig);
	free(buffer);
}
TEST(konfig2, konfig2_parse_line_allocs) {
	konfig2_t konfig = { 0 };

	konfig2_init_allocs(&konfig, 1000);

	const char *key1 = "key1";
	long data1 = 42;
	konfig2_var_new_long_allocs(&konfig, (char*)key1, &data1);
	const char *key2 = "key2";
	double data2 = 42.3;
	konfig2_var_new_double_allocs(&konfig, (char*)key2, &data2);
	const char *key3 = "key3";
	char data3[1024];
	sprintf(data3, "%s", "fortytwo");
	konfig2_var_new_string_allocs(&konfig, (char*)key3, data3, strlen(data3));

	const char *line = "key1=84";
	int result = konfig2_parse_line_allocs(&konfig, (char*)line, strlen(line));
	ASSERT_EQ(0, result);



	konfig2_release_frees(&konfig);
}
TEST(konfig2, konfig2_parse_document_allocs) {


	konfig2_t konfig = { 0 };

	konfig2_init_allocs(&konfig, 1000);

	const char *key1 = "key1";
	long data1 = 0;
	konfig2_var_new_long_allocs(&konfig, (char*)key1, &data1);
	const char *key2 = "key2";
	double data2 = 0;
	konfig2_var_new_double_allocs(&konfig, (char*)key2, &data2);
	const char *key3 = "key3";
	char data3[1024];
	sprintf(data3, "%s", "zero");
	konfig2_var_new_string_allocs(&konfig, (char*)key3, data3, sizeof(data3));

	const char *buffer = "key2=25.6\nkey3=fortytwo\nkey1=42\n";
	konfig2_parse_document_allocs(&konfig, (char*)buffer, strlen(buffer));

	ASSERT_EQ(25.6, data2);
	ASSERT_EQ(42, data1);
	ASSERT_EQ(0, strcmp("fortytwo", data3));


	konfig2_release_frees(&konfig);
}
