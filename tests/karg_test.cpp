#include <gtest/gtest.h>
#include <atomic>
using namespace std;

extern "C" {
    #include <karg.h>
    #include <malloc.h>
    #include <string.h>
}


TEST(karg, karg_build_getopts_allocs) {
	karg_t args;
	karg_option_t public_network_address;
	karg_option_t listen_address;
	karg_option_t seed_address;
	karg_option_t local_node;




	karg_init(&args);
	strcpy(args.author, "jkushmaul");
	strcpy(args.program_name, "test");


	karg_option_init(&args.options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
	karg_option_init(&args.options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
	karg_option_init(&args.options, &listen_address, 'l', (char*)"listen", (char*)"Local address to listen. If not provided uses public", 0, NULL, 1);
	karg_option_init(&args.options, &local_node, 'n', (char*)"node", (char*)"Local node id.  If not provided is generated", 0, NULL, 1);


	karg_getopt_option_def *def = karg_build_getopts_allocs(&args);

	ASSERT_EQ(0, strcmp(def->short_options, ":p:s:l:n:"));
	ASSERT_EQ(4, def->optlen);

	struct option *o;
	o = &def->long_options[0];
	ASSERT_EQ(1, o->has_arg);
	ASSERT_EQ(0, o->flag);
	ASSERT_EQ('p', o->val);
	ASSERT_EQ(0, strcmp(o->name, "public"));

	o = &def->long_options[3];
	ASSERT_EQ(1, o->has_arg);
	ASSERT_EQ(0, o->flag);
	ASSERT_EQ('n', o->val);
	ASSERT_EQ(0, strcmp(o->name, "node"));

	o = &def->long_options[4];
	ASSERT_EQ(0, o->has_arg);
	ASSERT_EQ(0, o->flag);
	ASSERT_EQ(0, o->val);
	ASSERT_EQ(0, o->name);

	free(def);
	karg_release_frees(&args);
}

TEST(karg, karg_parse_args__pass) {
	karg_t args;
	karg_option_t public_network_address;
	karg_option_t listen_address;
	karg_option_t seed_address;
	karg_option_t local_node;




	karg_init(&args);
	strcpy(args.author, "jkushmaul");
	strcpy(args.program_name, "test");


	karg_option_init(&args.options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
	karg_option_init(&args.options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
	karg_option_init(&args.options, &listen_address, 'l', (char*)"listen", (char*)"Local address to listen. If not provided uses public", 0, NULL, 1);
	karg_option_init(&args.options, &local_node, 'n', (char*)"node", (char*)"Local node id.  If not provided is generated", 0, NULL, 1);
	int argc = 5;

	const char *argv[5] = { "test", "-p", "127.0.0.1:31337", "-s", "127.0.0.2:31337" };

	karg_getopt_option_def *def = karg_build_getopts_allocs(&args);


	int result;
	result = karg_parse_args(&args, def, argc, (char**)argv);
	ASSERT_EQ(5, result);
	ASSERT_EQ(argv[2], public_network_address.value);
	ASSERT_EQ(argv[4], seed_address.value);
	free(def);
	karg_release_frees(&args);
}


TEST(karg, karg_parse_args__fail_required_value) {
	karg_t args;
	karg_option_t public_network_address;
	karg_option_t listen_address;
	karg_option_t seed_address;
	karg_option_t local_node;




	karg_init(&args);
	strcpy(args.author, "jkushmaul");
	strcpy(args.program_name, "test");


	karg_option_init(&args.options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
	karg_option_init(&args.options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
	karg_option_init(&args.options, &listen_address, 'l', (char*)"listen", (char*)"Local address to listen. If not provided uses public", 0, NULL, 1);
	karg_option_init(&args.options, &local_node, 'n', (char*)"node", (char*)"Local node id.  If not provided is generated", 0, NULL, 1);
	int argc = 4;

	const char *argv[4] = { "test", "-p", "127.0.0.1:31337", "-s" };

	karg_getopt_option_def *def = karg_build_getopts_allocs(&args);


	int result;
	result = karg_parse_args(&args, def, argc, (char**)argv);
	ASSERT_EQ(-1, result);
	free(def);
	karg_release_frees(&args);
}



TEST(karg, karg_parse_args__fail_required_option_missing) {
	karg_t args;
	karg_option_t public_network_address;
	karg_option_t listen_address;
	karg_option_t seed_address;
	karg_option_t local_node;




	karg_init(&args);
	strcpy(args.author, "jkushmaul");
	strcpy(args.program_name, "test");


	karg_option_init(&args.options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
	karg_option_init(&args.options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
	karg_option_init(&args.options, &listen_address, 'l', (char*)"listen", (char*)"Local address to listen. If not provided uses public", 0, NULL, 1);
	karg_option_init(&args.options, &local_node, 'n', (char*)"node", (char*)"Local node id.  If not provided is generated", 0, NULL, 1);
	int argc = 5;

	const char *argv[5] = { "test", "-p", "127.0.0.1:31337", "-l", "127.0.0.1:31337" };

	karg_getopt_option_def *def = karg_build_getopts_allocs(&args);


	int result;
	result = karg_parse_args(&args, def, argc, (char**)argv);
	//nothing was wrong with present args - but validation will fail because of missing '-s'
	ASSERT_EQ(5, result);

	result = karg_validate_args(&args);

	ASSERT_EQ(-1, result);
	free(def);
	karg_release_frees(&args);
}



TEST(karg, karg_parse_as_getopts__fail_unkown_option) {
	karg_t args;
	karg_option_t public_network_address;
	karg_option_t listen_address;
	karg_option_t seed_address;
	karg_option_t local_node;




	karg_init(&args);
	strcpy(args.author, "jkushmaul");
	strcpy(args.program_name, "test");


	karg_option_init(&args.options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
	karg_option_init(&args.options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
	karg_option_init(&args.options, &listen_address, 'l', (char*)"listen", (char*)"Local address to listen. If not provided uses public", 0, NULL, 1);
	karg_option_init(&args.options, &local_node, 'n', (char*)"node", (char*)"Local node id.  If not provided is generated", 0, NULL, 1);
	int argc = 5;

	const char *argv[5] = { "test", "-p", "127.0.0.1:31337", "-z", "127" };

	karg_getopt_option_def *def = karg_build_getopts_allocs(&args);


	int result;
	result = karg_parse_args(&args, def, argc, (char**)argv);
	ASSERT_EQ(-1, result);
	free(def);
	karg_release_frees(&args);
}



TEST(karg, karg_parse_as_getopts__pass_operand) {
	karg_t args;
	karg_option_t public_network_address;
	karg_option_t listen_address;
	karg_option_t seed_address;
	karg_option_t local_node;




	karg_init(&args);
	strcpy(args.author, "jkushmaul");
	strcpy(args.program_name, "test");


	karg_option_init(&args.options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
	karg_option_init(&args.options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
	karg_option_init(&args.options, &listen_address, 'l', (char*)"listen", (char*)"Local address to listen. If not provided uses public", 0, NULL, 1);
	karg_option_init(&args.options, &local_node, 'n', (char*)"node", (char*)"Local node id.  If not provided is generated", 0, NULL, 1);
	int argc = 4;

	const char *argv[4] = { "test", "operand", "-s", "127.0.0.1:31337" };

	karg_getopt_option_def *def = karg_build_getopts_allocs(&args);

	const char *operand = argv[1];
	int result;
	result = karg_parse_args(&args, def, argc, (char**)argv);
	ASSERT_EQ(3, result);
	ASSERT_EQ(operand, argv[result]);
	free(def);
	karg_release_frees(&args);
}

TEST(karg, karg_parse_as_getopts__pass_command) {
	karg_t args;
	karg_option_t public_network_address;
	karg_option_t listen_address;
	karg_option_t seed_address;
	karg_option_t local_node;




	karg_init(&args);
	strcpy(args.author, "jkushmaul");
	strcpy(args.program_name, "test");


	karg_option_init(&args.options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
	karg_option_init(&args.options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
	karg_option_init(&args.options, &listen_address, 'l', (char*)"listen", (char*)"Local address to listen. If not provided uses public", 0, NULL, 1);
	karg_option_init(&args.options, &local_node, 'n', (char*)"node", (char*)"Local node id.  If not provided is generated", 0, NULL, 1);
	karg_command_t init;
	karg_command_init(&args.commands, &init, (char*)"init", (char*)"Help", NULL);
	int argc = 4;

	const char *argv[4] = { "test", "init", "-s", "127.0.0.1:31337" };

	karg_getopt_option_def *def = karg_build_getopts_allocs(&args);


	int result;
	result = karg_parse_args(&args, def, argc, (char**)argv);
	ASSERT_EQ(4, result);
	free(def);
	karg_release_frees(&args);
}


TEST(karg, karg_parse_as_getopts__pass_command_with_operands) {
	karg_t args;
	karg_option_t public_network_address;
	karg_option_t listen_address;
	karg_option_t seed_address;
	karg_option_t local_node;




	karg_init(&args);
	strcpy(args.author, "jkushmaul");
	strcpy(args.program_name, "test");


	karg_option_init(&args.options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
	karg_option_init(&args.options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
	karg_option_init(&args.options, &listen_address, 'l', (char*)"listen", (char*)"Local address to listen. If not provided uses public", 0, NULL, 1);
	karg_option_init(&args.options, &local_node, 'n', (char*)"node", (char*)"Local node id.  If not provided is generated", 0, NULL, 1);
	karg_command_t init;
	karg_command_init(&args.commands, &init, (char*)"init", (char*)"Help", NULL);
	int argc = 6;

	const char *argv[6] = { "test", "init", "operand1", "-s", "127.0.0.1:31337", "operand2" };

	karg_getopt_option_def *def = karg_build_getopts_allocs(&args);


	int result;
	const char *operand1 = argv[2];
	const char *operand2 = argv[5];
	result = karg_parse_args(&args, def, argc, (char**)argv);
	ASSERT_EQ(4, result);
	ASSERT_EQ(operand1, argv[4]);
	ASSERT_EQ(operand2, argv[5]);
	free(def);
	karg_release_frees(&args);
}



TEST(karg, karg_validate_args){
	karg_t args;

	karg_init(&args);

	karg_option_t vopt;
	karg_option_init(&args.options, &vopt, 'v', (char*)"verbose", (char*)"Increase verbosity with each repeated parameter", 0, NULL, 0);
	vopt.argv_index = 0;

	//now a command with one contextual parameter "d"
	karg_command_t command;
	karg_command_init(&args.commands, &command, (char*)"init", (char*)"Initializes something", NULL);
	command.argv_index = 1;

	karg_option_t vopt2;
	karg_option_init(&command.options, &vopt2, 'd', (char*)"directory", (char*)"Use directory for initialization", 1, NULL, 1);

	int result;
	result = karg_validate_args(&args);
	ASSERT_EQ(-1, result);


	vopt2.argv_index = 2;
	result = karg_validate_args(&args);
	ASSERT_EQ(0, result);

	vopt.required = 1;
	result = karg_validate_args(&args);
	ASSERT_EQ(-1, result);



	karg_release_frees(&args);
}

TEST(karg, karg_print_help) {
	karg_t args;

	karg_option_t public_network_address;
	karg_option_t listen_address;
	karg_option_t seed_address;
	karg_option_t local_node;


	karg_init(&args);
	strcpy(args.author, "jkushmaul");
	strcpy(args.program_name, "test");
	args.errors.has_error = 1;
	strcpy(args.errors.error_msg, "Some error");

	karg_option_init(&args.options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
	karg_option_init(&args.options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
	karg_option_init(&args.options, &listen_address, 'l', (char*)"listen", (char*)"Local address to listen. If not provided uses public", 0, NULL, 1);
	karg_option_init(&args.options, &local_node, 'n', (char*)"node", (char*)"Local node id.  If not provided is generated", 0, NULL, 1);

	karg_print_help(&args);
	karg_release_frees(&args);
	ASSERT_EQ(0, 0);
}
